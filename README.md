# Introduction

I'm Dr Sivasothy SHANMUGALINGM, worked on network and systems more than 10 years from communciation services, IP/MPLS, SDN, NFV,cloud,  4G/5G/IoT  to security(DTLS, OSCORE LAKE) with IoT network. I do development in C, Python and GO. 

Since this month, I'm looking for a new job in the field of network and system. 

In the last job, when I was reviewing the IoT security protocols for the integration with IoT network along E2E IP connection, I started looking at the Quantum Computing. In this context, I found that Shor's and Grover's algorithms can break the current security system (i.e. AES).

# Experience with Quantum Computing/Quantum Networking

Obviously, I have very basic knowledge in this field and have zero knowledge in physics level. Some concepts such as entaglement, teleporation are yet to touch my heart. I follow the proposal of architecture principal for a [Quantum Internet at IRTF](https://tools.ietf.org/id/draft-irtf-qirg-principles-00.html) and gain higher level understanding of [the six-stage plan for realizing the Quantum Internet, written by Stephanie Wehner et al](https://science.sciencemag.org/content/362/6412/eaam9288.full). recently, I played with [Simulaqron simulator](http://www.simulaqron.org/) with BB84 Quantum Key distribution protocol. 

# What to do in this hackathon (PEQI2019)

According to the introduction presentation of the PEQI Hackthon, there are two options for participants: 

- contribute to integration of openSSL with QKD. I have good understanding of openSSL and C programming. I'm not sure that Paris node will work on this topic.  
- open-ended challenge from Quantum Protocol Zoo. In this case, it is possible to implement Quantum protocol using SimulaQron or develop applications that use of these protocols.

After reading the [excel sheet](https://docs.google.com/spreadsheets/d/1jiNrwHp9mOP1rv2L70fuviRThhhmLrhrLqDbcOx4pLM/edit?invite=CPfv_qoL&ts=5dbab30f#gid=0), some protocols are not implemented. So I'm thinking of implementing [Fast Quantum Byzantine Agreement](https://wiki.veriqloud.fr/index.php?title=Fast_Quantum_Byzantine_Agreement) in SimulaQron. Given that I know neither Byzantine Agreement nor Quantum protocol. 

I'm not sure that what protocol already implemented is useful basis for implementing Fast Quantum Byzantine Agreement, which will be possible at the last stage of the development of a Quantum Internet.
Beffore developing the solution, I should look into simple leader election and agreement protocols. Two implementation are basis also: Oblivious Common Coin and Verifiable Quantum Secret Sharing( VQSS).  

 

  
 


In this context, if someone leads to define the tasks for implementing Fast Quantum Byzatine agreement, I can contribute in this direction. 


In the meantime, I'm ready to work on others idea as well.

# Contribution

At the Hackthon, I worked with team who are interested to validate algorithm - distributing Graph states over Arbitary Quantum Network  using Simulaqron simulator. 
The original paper describing the algorithm is available [here](https://arxiv.org/pdf/1811.05445.pdf). The outcome of work is made online with more description in [github](https://github.com/francois-marie/StarExpanders). The validation pinpoints one shortcoming of the algorithm. Quantum Error correction technique is not included in the algorithm.
         
